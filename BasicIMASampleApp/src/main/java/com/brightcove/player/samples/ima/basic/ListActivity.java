package com.brightcove.player.samples.ima.basic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by kdimla on 6/1/15.
 */
public class ListActivity extends Activity {
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.ima_activity_list);
        super.onCreate(savedInstanceState);
        mButton = (Button) findViewById(R.id.ima_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this, MainActivity.class));
            }
        });

        String listItemText = "List Item";
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);
        arrayList.add(listItemText);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        ListView listView = (ListView) findViewById(R.id.ima_listview);
        listView.setAdapter(arrayAdapter);
    }
}
